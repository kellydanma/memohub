package rand

import (
	"testing"

	"github.com/stretchr/testify/require"
)

var (
	testString = "abcdeabcdeabcdeabcdeabcdeabcdeab"
)

func TestRememberToken(t *testing.T) {
	_, err := RememberToken()
	require.NoError(t, err)
}

func TestString(t *testing.T) {
	_, err := String(10)
	require.NoError(t, err)
	_, err = String(0)
	require.NoError(t, err)
}

func TestNrBytes(t *testing.T) {
	bytes, err := NrBytes(testString)
	require.Equal(t, bytes, 24)
	require.NoError(t, err)
}
