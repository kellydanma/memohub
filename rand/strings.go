package rand

import (
	"crypto/rand"
	"encoding/base64"
)

// RTokenBytes specifies 32 bytes for a remember token.
const RTokenBytes = 32

// RememberToken generates a remember token of size 32.
func RememberToken() (string, error) {
	return String(RTokenBytes)
}

// Bytes generates n random bytes.
func Bytes(n int) ([]byte, error) {
	b := make([]byte, n)
	_, err := rand.Read(b)
	if err != nil {
		return nil, err
	}
	return b, nil
}

// String generates a byte slice of size nrBytes
// and returns a base64 URL encoded string of that slice.
func String(nrBytes int) (string, error) {
	b, err := Bytes(nrBytes)
	if err != nil {
		return "", err
	}
	return base64.URLEncoding.EncodeToString(b), nil
}

// NrBytes returns the number of bytes used in
// the string generated in this package.
// It is essentially the opposite of the String()
// function.
func NrBytes(base64String string) (int, error) {
	b, err := base64.URLEncoding.DecodeString(base64String)
	if err != nil {
		return 0, err
	}
	return len(b), nil
}
