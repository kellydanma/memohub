package controllers

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"github.com/kellydanma/memohub/context"
	"github.com/kellydanma/memohub/models"
	"github.com/kellydanma/memohub/views"
)

const (
	// IndexCollections is the route for Index.
	IndexCollections = "index_collections"
	// ShowCollection is the route for Show.
	ShowCollection = "show_collection"
	// EditCollection is the route for Edit.
	EditCollection = "edit_collection"

	maxMultipartMem = 1 << 20 // 1 megabyte
)

// NewCollections is used to create a new Collections
// controller. This function will panic if
// the templates are not parsed correctly;
// it should only be used during initial setup.
func NewCollections(cs models.CollectionService, is models.ImageService, r *mux.Router) *Collections {
	newView, err := views.NewView("bootstrap", "collections/new")
	if err != nil {
		panic(err)
	}
	showView, err := views.NewView("bootstrap", "collections/show")
	if err != nil {
		panic(err)
	}
	editView, err := views.NewView("bootstrap", "collections/edit")
	if err != nil {
		panic(err)
	}
	indexView, err := views.NewView("bootstrap", "collections/index")
	if err != nil {
		panic(err)
	}
	return &Collections{
		New:       newView,
		ShowView:  showView,
		EditView:  editView,
		IndexView: indexView,
		cs:        cs,
		is:        is,
		rt:        r,
	}
}

// Collections is a controller that handles all
// resources related to collections.
type Collections struct {
	New       *views.View
	ShowView  *views.View
	EditView  *views.View
	IndexView *views.View
	cs        models.CollectionService
	is        models.ImageService
	rt        *mux.Router
}

// CollectionForm contains the title of a new
// collection to be created.
type CollectionForm struct {
	Title string `schema:"title"`
}

// Index displays a user's collections
// GET /collections
func (c *Collections) Index(w http.ResponseWriter, r *http.Request) {
	user := context.User(r.Context())
	collections, err := c.cs.ByUserID(user.ID)
	if err != nil {
		http.Error(w, "Something went wrong.", http.StatusInternalServerError)
		return
	}
	var vData views.Data
	vData.Display = collections
	c.IndexView.Render(w, r, vData)
}

// Show renders the current collection.
// GET /collections/:id
func (c *Collections) Show(w http.ResponseWriter, r *http.Request) {
	collection, err := c.collectionByID(w, r)
	if err != nil {
		return // collectionByID already renders the error
	}
	var vData views.Data
	vData.Display = collection
	c.ShowView.Render(w, r, vData)
}

// ImageUpload uploads images to a collection.
// POST /collections/:id/images
func (c *Collections) ImageUpload(w http.ResponseWriter, r *http.Request) {
	collection, err := c.collectionByID(w, r)
	if err != nil {
		return
	}
	user := context.User(r.Context())
	if collection.UserID != user.ID {
		http.Error(w, "Collection not found", http.StatusNotFound)
		return
	}

	var vData views.Data
	vData.Display = collection
	err = r.ParseMultipartForm(maxMultipartMem)
	if err != nil {
		vData.SetAlert(err)
		c.EditView.Render(w, r, vData)
		return
	}

	// Iterate over uploaded files to process them
	files := r.MultipartForm.File["images"]
	for _, f := range files {
		// Open the uploaded file
		file, err := f.Open()
		if err != nil {
			vData.SetAlert(err)
			c.EditView.Render(w, r, vData)
			return
		}
		defer file.Close()

		// Create the image
		err = c.is.Create(collection.ID, file, f.Filename)
		if err != nil {
			vData.SetAlert(err)
			c.EditView.Render(w, r, vData)
			return
		}
	}

	vData.Alert = &views.Alert{
		Level:   views.AlertLevelSuccess,
		Message: "Images successfully uploaded!",
	}
	c.EditView.Render(w, r, vData)
}

// ImageDelete deletes an image from the collection.
// POST /collections/:id/images/:filename/delete
func (c *Collections) ImageDelete(w http.ResponseWriter, r *http.Request) {
	collection, err := c.collectionByID(w, r)
	if err != nil {
		return
	}
	user := context.User(r.Context())
	if collection.UserID != user.ID {
		http.Error(w, "You do not have permission to edit "+
			"this collection or image", http.StatusForbidden)
		return
	}
	filename := mux.Vars(r)["filename"]
	i := models.Image{
		Filename:     filename,
		CollectionID: collection.ID,
	}
	err = c.is.Delete(&i)
	if err != nil {
		var vd views.Data
		vd.Display = collection
		vd.SetAlert(err)
		c.EditView.Render(w, r, vd)
		return
	}
	url, err := c.rt.Get(EditCollection).URL("id", fmt.Sprintf("%v", collection.ID))
	if err != nil {
		http.Redirect(w, r, "/collections", http.StatusFound)
		return
	}
	http.Redirect(w, r, url.Path, http.StatusFound)
}

// Create is used to process the new collection form
// when a user creates a new collection.
// POST /collections
func (c *Collections) Create(w http.ResponseWriter, r *http.Request) {
	var form CollectionForm
	var vData views.Data
	if err := parseForm(r, &form); err != nil {
		vData.SetAlert(err)
		c.New.Render(w, r, vData)
		return
	}
	user := context.User(r.Context()) // get user from context
	collection := models.Collection{
		Title:  form.Title,
		UserID: user.ID,
	}
	if err := c.cs.Create(&collection); err != nil {
		vData.SetAlert(err)
		c.New.Render(w, r, vData)
		return
	}
	url, err := c.rt.Get(EditCollection).URL("id", strconv.Itoa(int(collection.ID)))
	if err != nil {
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}
	http.Redirect(w, r, url.Path, http.StatusFound)
}

// Edit allows a user to edit their collection.
// GET /collections/:id/edit
func (c *Collections) Edit(w http.ResponseWriter, r *http.Request) {
	collection, err := c.collectionByID(w, r)
	if err != nil {
		return // collectionByID already renders the error
	}
	// check that the correct user is logged in
	user := context.User(r.Context())
	if collection.UserID != user.ID {
		http.Error(w, "You do not have permission to edit this collection", http.StatusForbidden)
		return
	}
	var vData views.Data
	vData.Display = collection
	c.EditView.Render(w, r, vData)
}

// Update updates a collection.
// POST /collections/:id/update
func (c *Collections) Update(w http.ResponseWriter, r *http.Request) {
	collection, err := c.collectionByID(w, r)
	if err != nil {
		return // collectionByID already renders the error
	}
	user := context.User(r.Context()) // check that the correct user is logged in
	if collection.UserID != user.ID {
		http.Error(w, "Collection not found", http.StatusNotFound)
		return
	}
	var vData views.Data
	vData.Display = collection
	var form CollectionForm
	if err = parseForm(r, &form); err != nil {
		vData.SetAlert(err)
		c.EditView.Render(w, r, vData)
		return
	}
	collection.Title = form.Title
	err = c.cs.Update(collection)
	if err != nil {
		vData.SetAlert(err)
		c.EditView.Render(w, r, vData)
		return
	}
	vData.Alert = &views.Alert{
		Level:   views.AlertLevelSuccess,
		Message: "Collection successfully updated!",
	}
	c.EditView.Render(w, r, vData)
}

// Delete removes a collection.
// POST /collections/:id/delete
func (c *Collections) Delete(w http.ResponseWriter, r *http.Request) {
	collection, err := c.collectionByID(w, r)
	if err != nil {
		return
	}
	user := context.User(r.Context())
	if collection.UserID != user.ID {
		http.Error(w, "You do not have permission to edit "+
			"this collection", http.StatusForbidden)
		return
	}
	var vData views.Data
	err = c.cs.Delete(collection.ID)
	if err != nil {
		vData.SetAlert(err)
		vData.Display = collection
		c.EditView.Render(w, r, vData)
		return
	}
	url, err := c.rt.Get(IndexCollections).URL()
	if err != nil {
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}
	http.Redirect(w, r, url.Path, http.StatusFound)
}

// collectionByID is a helper that retrieves a Collection by its ID
func (c *Collections) collectionByID(w http.ResponseWriter, r *http.Request) (*models.Collection, error) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		http.Error(w, "Invalid collection ID", http.StatusNotFound)
		return nil, err
	}
	collection, err := c.cs.ByID(uint(id))
	if err != nil {
		switch err {
		case models.ErrNotFound:
			http.Error(w, "Collection not found", http.StatusNotFound)
		default:
			http.Error(w, "Oops, something went wrong!", http.StatusInternalServerError)
		}
		return nil, err
	}
	images, _ := c.is.ByCollectionID(collection.ID)
	collection.Images = images
	return collection, nil
}
