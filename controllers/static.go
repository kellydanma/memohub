package controllers

import "github.com/kellydanma/memohub/views"

// Static is a controller for MemoHub's
// static pages, such as the landing page.
type Static struct {
	Home    *views.View
	Contact *views.View
	FAQ     *views.View
}

// NewStatic creates a new controller for
// Memohub's static pages.
// MemoHub cannot start without these pages,
// so NewStatic panics if an error occurs.
func NewStatic() *Static {
	homeView, err := views.NewView("bootstrap", "static/home")
	if err != nil {
		panic(err)
	}
	contactView, err := views.NewView("bootstrap", "static/contact")
	if err != nil {
		panic(err)
	}
	faqView, err := views.NewView("bootstrap", "static/faq")
	if err != nil {
		panic(err)
	}
	return &Static{
		Home:    homeView,
		Contact: contactView,
		FAQ:     faqView,
	}
}
