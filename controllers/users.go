package controllers

import (
	"fmt"
	"net/http"

	"github.com/kellydanma/memohub/models"
	"github.com/kellydanma/memohub/rand"

	"github.com/kellydanma/memohub/views"
)

// NewUsers is used to create a new Users
// controller. This function will panic if
// the templates are not parsed correctly;
// it should only be used during initial setup.
func NewUsers(us models.UserService) *Users {
	newView, err := views.NewView("bootstrap", "users/new")
	if err != nil {
		panic(err)
	}
	loginView, err := views.NewView("bootstrap", "users/login")
	if err != nil {
		panic(err)
	}
	return &Users{
		NewView:   newView,
		LogInView: loginView,
		us:        us,
	}
}

// Users is a controller that handles all
// resources related to users.
type Users struct {
	NewView   *views.View
	LogInView *views.View
	us        models.UserService
}

// SignupForm contains a user's signup
// details: name, e-mail, and password.
type SignupForm struct {
	Name     string `schema:"name"`
	Email    string `schema:"email"`
	Password string `schema:"password"`
}

// LogInForm contains a user's login
// details: e-mail and password.
type LogInForm struct {
	Email    string `schema:"email"`
	Password string `schema:"password"`
}

// New is used to render the form where a
// user creates a new account.
// GET /signup
func (u *Users) New(w http.ResponseWriter, r *http.Request) {
	u.NewView.Render(w, r, nil)
}

// Create is used to process the signup form
// when a user creates a new account.
// POST /signup
func (u *Users) Create(w http.ResponseWriter, r *http.Request) {
	var form SignupForm
	var vData views.Data
	if err := parseForm(r, &form); err != nil {
		vData.SetAlert(err)
		u.NewView.Render(w, r, vData)
		return
	}
	user := models.User{
		Name:     form.Name,
		Email:    form.Email,
		Password: form.Password,
	}
	if err := u.us.Create(&user); err != nil {
		vData.SetAlert(err)
		u.NewView.Render(w, r, vData)
		return
	}
	err := u.signIn(w, &user)
	if err != nil {
		http.Redirect(w, r, "/login", http.StatusFound)
		return
	}
	http.Redirect(w, r, "/collections", http.StatusFound)
}

// LogIn is used to process the login form
// when a user signs into an existing account.
// POST /login
func (u *Users) LogIn(w http.ResponseWriter, r *http.Request) {
	var form LogInForm
	var vData views.Data
	if err := parseForm(r, &form); err != nil {
		vData.SetAlert(err)
		u.LogInView.Render(w, r, vData)
		return
	}
	user, err := u.us.Authenticate(form.Email, form.Password)
	if err != nil {
		switch err {
		case models.ErrNotFound:
			vData.AlertError("User does not exist")
		default:
			vData.SetAlert(err)
		}
		u.LogInView.Render(w, r, vData)
		return
	}
	err = u.signIn(w, user)
	if err != nil {
		vData.SetAlert(err)
		u.LogInView.Render(w, r, vData)
		return
	}
	http.Redirect(w, r, "/collections", http.StatusFound)
}

// CookieTest displays the cookies set on the current user.
func (u *Users) CookieTest(w http.ResponseWriter, r *http.Request) {
	cookie, err := r.Cookie("remember_token")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	user, err := u.us.ByRemember(cookie.Value)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	fmt.Fprintln(w, user)
}

// signIn signs the given user in via cookies.
func (u *Users) signIn(w http.ResponseWriter, user *models.User) error {
	if user.Remember == "" {
		token, err := rand.RememberToken()
		if err != nil {
			return err
		}
		user.Remember = token
		err = u.us.Update(user)
		if err != nil {
			return err
		}
	}
	cookie := http.Cookie{
		Name:     "remember_token",
		Value:    user.Remember,
		HttpOnly: true,
	}
	http.SetCookie(w, &cookie)
	return nil
}
