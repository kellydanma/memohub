package main

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/kellydanma/memohub/controllers"
	"github.com/kellydanma/memohub/middleware"
	"github.com/kellydanma/memohub/models"
)

const (
	host     = "localhost"
	port     = 5432
	user     = "postgres"
	password = "memodev"
	dbname   = "memohub_dev"
)

func main() {
	// Create a DB connection string and then use it to
	// initiate services.
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)

	services, err := models.NewServices(psqlInfo)
	if err != nil {
		panic(err)
	}
	defer services.Close()
	services.Reset()
	services.AutoMigrate()

	// router & controllers setup
	r := mux.NewRouter()
	staticC := controllers.NewStatic()
	usersC := controllers.NewUsers(services.User)
	collectionsC := controllers.NewCollections(services.Collection, services.Image, r)
	userMw := middleware.User{
		UserService: services.User,
	}
	requireUserMw := middleware.RequireUser{}

	// static routes
	r.Handle("/", staticC.Home).Methods("GET")
	r.Handle("/contact", staticC.Contact).Methods("GET")
	r.Handle("/faq", staticC.FAQ).Methods("GET")

	// user routes
	r.HandleFunc("/signup", usersC.New).Methods("GET")
	r.HandleFunc("/signup", usersC.Create).Methods("POST")
	r.Handle("/login", usersC.LogInView).Methods("GET")
	r.HandleFunc("/login", usersC.LogIn).Methods("POST")
	r.HandleFunc("/cookie", usersC.CookieTest).Methods("GET")

	// collection routes
	r.Handle("/collections",
		requireUserMw.ApplyFn(collectionsC.Index)).
		Methods("GET").
		Name(controllers.IndexCollections)
	r.Handle("/collections/new",
		requireUserMw.Apply(collectionsC.New)).
		Methods("GET")
	r.Handle("/collections",
		requireUserMw.ApplyFn(collectionsC.Create)).
		Methods("POST")
	r.HandleFunc("/collections/{id:[0-9]+}",
		collectionsC.Show).
		Methods("GET").
		Name(controllers.ShowCollection)
	r.HandleFunc("/collections/{id:[0-9]+}/edit",
		requireUserMw.ApplyFn(collectionsC.Edit)).
		Methods("GET").
		Name(controllers.EditCollection)
	r.HandleFunc("/collections/{id:[0-9]+}/update",
		requireUserMw.ApplyFn(collectionsC.Update)).
		Methods("POST")
	r.HandleFunc("/collections/{id:[0-9]+}/delete",
		requireUserMw.ApplyFn(collectionsC.Delete)).
		Methods("POST")
	r.HandleFunc("/collections/{id:[0-9]+}/images",
		requireUserMw.ApplyFn(collectionsC.ImageUpload)).
		Methods("POST")
	r.HandleFunc("/collections/{id:[0-9]+}/images/{filename}/delete",
		requireUserMw.ApplyFn(collectionsC.ImageDelete)).
		Methods("POST")

	// image routes
	imageHandler := http.FileServer(http.Dir("./images/"))
	r.PathPrefix("/images/").Handler(http.StripPrefix("/images/", imageHandler))

	// listen & serve
	fmt.Println("Starting the server on :8080 ...")
	http.ListenAndServe(":8080", userMw.Apply(r))
}
