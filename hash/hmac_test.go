package hash

import (
	"testing"

	"github.com/stretchr/testify/require"
)

var (
	testHMAC   HMAC
	testHash   = "g-Ln0ZNUHOeJOa1bUbdQ7nXTBzEb86rq3CzCG3Zr0KE="
	testKey    = "a-secret-key"
	testString = "my string to hash"
)

func TestHash(t *testing.T) {
	testNewHMAC(t)
	out := testHMAC.Hash(testString)
	require.Equal(t, out, testHash)
}

func testNewHMAC(t *testing.T) {
	t.Helper()
	testHMAC = NewHMAC(testKey)
}
