package hash

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"hash"
)

// HMAC is a wrapper for the crypto/hmac package.
type HMAC struct {
	hmac hash.Hash
}

// NewHMAC creates and returns a new HMAC object.
// HMAC or "hash-based message authentication code"
// is a specific type of message authentication code
// involving a cryptographic hash function and a
// secret cryptographic key.
func NewHMAC(key string) HMAC {
	h := hmac.New(sha256.New, []byte(key))
	return HMAC{
		hmac: h,
	}
}

// Hash hashes the input string using HMAC with the
// secret key provided when the HMAC object was created.
func (h HMAC) Hash(in string) string {
	h.hmac.Reset()
	h.hmac.Write([]byte(in))
	b := h.hmac.Sum(nil)
	return base64.URLEncoding.EncodeToString(b)
}
