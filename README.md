# MemoHub

This is a **WIP** image repository that supports `CRUD` operations, including bulk upload. The entire web application is written in Go with a bootstrapped frontend and integration to a PostgreSQL server database.

## Requirements

- Go 1.14
- PostgreSQL 10+

## Getting Started

```git
git clone https://github.com/kellydanma/memohub.git

# alternatively, use go get
go get github.com/kellydanma/memohub

# navigate to directory
cd ~/go/src/github.com/kellydanma/memohub

# run
go run cmd/main.go
```

Navigate to `localhost:8080` in browser to view the application.

## Routes

### Users

| URL | Method | Description |
|:---:|:------:|:-----------:|
|`/signup`|`GET`| Retrieves the signup form to register a new user |
|`/signup`|`POST`| Sends registration form data to the router |
|`/login`|`GET`| Retrieves the login form to sign in an existing user |
|`/login`|`POST`| Sends login form data to the router |

### Image Collections

| URL | Method | Description |
|:---:|:------:|:-----------:|
|`/collections`|`GET`| Displays all collections belonging to a signed-in user |
|`/collections/new`|`GET`| Retrieves the create form for a new collection |
|`/collections/new`|`POST`| Sends create form data to the router |
|`/collections/{id:[0-9]+}`|`GET`| Retrieves one collection of images |
|`/collections/{id:[0-9]+}/edit`|`GET`| Retrieves one collection of images in edit mode |
|`/collections/{id:[0-9]+}/update`|`POST`| Sends update collection form data to the router |
|`/collections/{id:[0-9]+}/delete`|`POST`| Deletes a collection of images |

### Images

| URL | Method | Description |
|:---:|:------:|:-----------:|
|`/collections/{id:[0-9]+}/images`|`POST`| Bulk uploads images to a collection |
|`/collections/{id:[0-9]+}/images/{filename}/delete`|`POST`| Deletes one image from a collection |

### Static

## Models

The models used in this project are [gorm](https://gorm.io/)-based. For simplicity, all `gorm` tags have been removed below, in addition to password-hashing fields.

### User

```go
type User struct {
	Name         string
	Email        string
	Password     string
}
```

### Collection

```go
type Collection struct {
	UserID uint
	Title  string
	Images []Image
}
```

### Image

```go
type Image struct {
	CollectionID uint
	Filename     string
}
```

## To Do

- [x] MVC implementation of users
- [x] User authentication & validation
- [x] Bootstrap application and creat .gohtml templates
- [x] Provide UI rendering of alerts and support
- [x] MVC implementation of collections
- [x] MVC implementation of images
- [ ] Create an informative landing page (currently a stub)
- [ ] Add labelled descriptions to images
- [ ] Allow users to update their credentials & information
- [ ] Sync images to Google Drive or Dropbox
