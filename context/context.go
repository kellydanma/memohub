package context

import (
	"context"

	"github.com/kellydanma/memohub/models"
)

type privateKey string

const (
	userKey privateKey = "user"
)

// WithUser returns a new context with the given
// user as a value for the userKey.
func WithUser(ctx context.Context, user *models.User) context.Context {
	return context.WithValue(ctx, userKey, user)
}

// User obtains the a user from a given context.
func User(ctx context.Context) *models.User {
	if temp := ctx.Value(userKey); temp != nil {
		if user, ok := temp.(*models.User); ok {
			return user
		}
	}
	return nil
}
