package views

import (
	"bytes"
	"html/template"
	"io"
	"net/http"
	"path/filepath"

	"github.com/kellydanma/memohub/context"
)

var (
	layoutDir   string = "views/layouts/"
	templateDir string = "views/"
	templateExt string = ".gohtml"
)

// View contains an HTML template to render data.
type View struct {
	Template *template.Template
	Layout   string
}

// NewView returns a pointer to View given one or
// several strings as input.
// It panics if template parsing fails.
func NewView(layout string, files ...string) (*View, error) {
	addTemplatePath(files)
	addTemplateExt(files)
	files = append(files, templateFiles()...)
	t, err := template.ParseFiles(files...)
	if err != nil {
		return nil, err
	}
	return &View{
		Template: t,
		Layout:   layout,
	}, nil
}

// Render handles rendering logic to display a View.
func (v *View) Render(w http.ResponseWriter, r *http.Request, data interface{}) {
	w.Header().Set("Content-Type", "text/html")
	var vData Data
	switch d := data.(type) {
	case Data:
		vData = d
	default:
		vData = Data{
			Display: data,
		}
	}
	vData.User = context.User(r.Context())
	var buf bytes.Buffer
	err := v.Template.ExecuteTemplate(&buf, v.Layout, vData)
	if err != nil {
		http.Error(w, "Something went wrong.",
			http.StatusInternalServerError)
		return
	}
	io.Copy(w, &buf)
}

// ServeHTTP handles logic to display static pages
// in MemoHub; it allows View to implement the
// http.Handler interface.
func (v *View) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	v.Render(w, r, nil)
}

// templateFiles returns a slice of bootstrapped
// and styling files.
func templateFiles() []string {
	templates, err := filepath.Glob(layoutDir + "*" + templateExt)
	if err != nil {
		panic(err)
	}
	return templates
}

// addTemplatePath prepends the templateDir
// to each string in the string slice.
// Eg. the input {"home"} would result in the output
// {"views/home"} given a templateDir == "views/"
func addTemplatePath(files []string) {
	for i, f := range files {
		files[i] = templateDir + f
	}
}

// addTemplateExt appends the templateExt
// to each string in the string slice.
// Eg. the input {"home"} would result in the
// output {"home.gohtml"} given a
// templateExt == ".gohtml"
func addTemplateExt(files []string) {
	for i, f := range files {
		files[i] = f + templateExt
	}
}
