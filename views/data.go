package views

import (
	"log"

	"github.com/kellydanma/memohub/models"
)

const (
	// AlertLevelError is the most severe alert level.
	AlertLevelError = "danger"
	// AlertLevelInfo provides helpful information.
	AlertLevelInfo = "info"
	// AlertLevelSuccess reassures the user that all went well.
	AlertLevelSuccess = "success"
	// AlertLevelWarning doublechecks with the user.
	AlertLevelWarning = "warning"

	// AlertGenericMessage is displayed when backend errors are encountered.
	AlertGenericMessage = "Something went wrong. Please try again."
)

// SetAlert creates an alert.
func (d *Data) SetAlert(err error) {
	var msg string
	if pErr, ok := err.(PublicError); ok {
		msg = pErr.Public()
	} else { // don't display error to user
		log.Println(err)
		msg = AlertGenericMessage
	}
	d.Alert = &Alert{
		Level:   AlertLevelError,
		Message: msg,
	}
}

// AlertError sets an alert message for errors.
func (d *Data) AlertError(msg string) {
	d.Alert = &Alert{
		Level:   AlertLevelError,
		Message: msg,
	}
}

// Data abstracts expected view data.
type Data struct {
	Alert   *Alert
	User    *models.User
	Display interface{}
}

// Alert renders boostrapped alert messages.
type Alert struct {
	Level   string
	Message string
}

// PublicError is displayed in the UI.
type PublicError interface {
	error
	Public() string
}
