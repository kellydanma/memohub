package models

import (
	"github.com/jinzhu/gorm"
)

const (
	// ErrUserIDRequired signifies the collection is missing a user ID.
	ErrUserIDRequired modelError = "models: user ID is required"
	// ErrTitleRequired signifies the collection is missing a title.
	ErrTitleRequired modelError = "models: title is required"
)

///////////////////////////
///                     ///
///     Interfaces      ///
///                     ///
///////////////////////////

// CollectionDB interacts with the collections database.
type CollectionDB interface {
	ByID(id uint) (*Collection, error)
	ByUserID(userID uint) ([]Collection, error)
	Create(collection *Collection) error
	Update(collection *Collection) error
	Delete(id uint) error
}

// NewCollectionService initiates the collection service.
func NewCollectionService(db *gorm.DB) CollectionService {
	return &collectionService{
		CollectionDB: &collectionValidator{
			CollectionDB: &collectionORM{
				db: db,
			},
		},
	}
}

// CollectionService offers methods to manipulate
// the collection model.
type CollectionService interface {
	CollectionDB
}

///////////////////////////
///                     ///
///       Structs       ///
///                     ///
///////////////////////////

// Collection is a resource that organizes images.
type Collection struct {
	gorm.Model
	UserID uint    `gorm:"not_null;index"`
	Title  string  `gorm:"not_null"`
	Images []Image `gorm:"-"`
}

// ImagesSplitN returns a 2D array of images
// in columnized format.
func (c *Collection) ImagesSplitN(n int) [][]Image {
	ret := make([][]Image, n)
	for i := 0; i < n; i++ {
		ret[i] = make([]Image, 0)
	}
	for i, img := range c.Images {
		bucket := i % n
		ret[bucket] = append(ret[bucket], img)
	}
	return ret
}

// collectionService abstracts db operations and
// services as an API for querying, creating,
// and updating collections.
type collectionService struct {
	CollectionDB
}

///////////////////////////
///                     ///
///    ORM functions    ///
///                     ///
///////////////////////////

// collectionORM interacts with the DB and fully
// implements the CollectionDB interface.
type collectionORM struct {
	db *gorm.DB
}

// ByID returns a collection with the specified ID.
func (cORM *collectionORM) ByID(id uint) (*Collection, error) {
	var collection Collection
	db := cORM.db.Where("id = ?", id)
	if err := first(db, &collection); err != nil {
		return nil, err
	}
	return &collection, nil
}

// ByUserID returns a collection with the specified user ID.
func (cORM *collectionORM) ByUserID(userID uint) ([]Collection, error) {
	var collections []Collection
	db := cORM.db.Where("user_id = ?", userID)
	if err := db.Find(&collections).Error; err != nil {
		return nil, err
	}
	return collections, nil
}

// Create creates a new collection.
func (cORM *collectionORM) Create(collection *Collection) error {
	return cORM.db.Create(collection).Error
}

// Update updates an existing collection.
func (cORM *collectionORM) Update(collection *Collection) error {
	return cORM.db.Save(collection).Error
}

// Delete removes the collection with the specified ID.
func (cORM *collectionORM) Delete(id uint) error {
	collection := Collection{Model: gorm.Model{ID: id}}
	return cORM.db.Delete(&collection).Error
}

///////////////////////////
///                     ///
///     Validation      ///
///                     ///
///////////////////////////

// Create creates a collection, then calls Create
// on the next db interface layer.
func (cv *collectionValidator) Create(collection *Collection) error {
	if err := runCollectionFns(collection,
		cv.userIDRequired,
		cv.titleRequired); err != nil {
		return err
	}
	return cv.CollectionDB.Create(collection)
}

// Update updates a collection, then calls Update
// on the next db interface layer.
func (cv *collectionValidator) Update(collection *Collection) error {
	if err := runCollectionFns(collection,
		cv.userIDRequired,
		cv.titleRequired); err != nil {
		return err
	}
	return cv.CollectionDB.Update(collection)
}

// Delete removes a collection, then calls Delete
// on the next db interface layer.
func (cv *collectionValidator) Delete(id uint) error {
	var collection Collection
	collection.ID = id
	if err := runCollectionFns(&collection,
		cv.nonZeroID); err != nil {
		return err
	}
	return cv.CollectionDB.Delete(collection.ID)
}

// collectionValidator normalizes & validates data before
// passing it to the next CollectionDB in the interface chain.
type collectionValidator struct {
	CollectionDB
}

// collectionValFn validates collection data.
type collectionValFn func(*Collection) error

func runCollectionFns(collection *Collection, fns ...collectionValFn) error {
	for _, fn := range fns {
		if err := fn(collection); err != nil {
			return err
		}
	}
	return nil
}

func (cv *collectionValidator) userIDRequired(c *Collection) error {
	if c.UserID <= 0 {
		return ErrUserIDRequired
	}
	return nil
}

func (cv *collectionValidator) titleRequired(c *Collection) error {
	if c.Title == "" {
		return ErrTitleRequired
	}
	return nil
}

func (cv *collectionValidator) nonZeroID(c *Collection) error {
	if c.ID <= 0 {
		return ErrIDInvalid
	}
	return nil
}
