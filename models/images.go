package models

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
)

// Image is used to represent images stored in a Collection.
// Image is NOT stored in the database, and instead
// references data stored on disk.
type Image struct {
	CollectionID uint
	Filename     string
}

// Path is used to build the absolute path used to
// reference this image via a web request.
func (i *Image) Path() string {
	return "/" + i.RelativePath()
}

// RelativePath is used to build the path to this image on our local
// disk, relative to where our Go application is run from.
func (i *Image) RelativePath() string {
	// Convert the collection ID to a string
	collectionID := fmt.Sprintf("%v", i.CollectionID)
	return filepath.Join("images", "galleries", collectionID, i.Filename)
}

// ImageService offers methods to manipulate
// the image model.
type ImageService interface {
	Create(collectionID uint, r io.Reader, filename string) error
	ByCollectionID(collectionID uint) ([]Image, error)
	Delete(i *Image) error
}

// NewImageService initiates the image service.
func NewImageService() ImageService {
	return &imageService{}
}

type imageService struct{}

// Create adds an image to the collection.
func (is *imageService) Create(collectionID uint, r io.Reader, filename string) error {
	path, err := is.mkImagePath(collectionID)
	if err != nil {
		return err
	}
	// Create a destination file
	dst, err := os.Create(filepath.Join(path, filename))
	if err != nil {
		return err
	}
	defer dst.Close()
	// Copy reader data to the destination file
	_, err = io.Copy(dst, r)
	if err != nil {
		return err
	}
	return nil
}

// ByCollectionID returns images belonging to
// the specific collection.
func (is *imageService) ByCollectionID(collectionID uint) ([]Image, error) {
	path := is.imagePath(collectionID)
	strings, err := filepath.Glob(filepath.Join(path, "*"))
	if err != nil {
		return nil, err
	}
	// Setup the Image slice we are returning
	ret := make([]Image, len(strings))
	for i, imgStr := range strings {
		ret[i] = Image{
			Filename:     filepath.Base(imgStr),
			CollectionID: collectionID,
		}
	}
	return ret, nil
}

// Delete removes an image file.
func (is *imageService) Delete(i *Image) error {
	return os.Remove(i.RelativePath())
}

func (is *imageService) imagePath(collectionID uint) string {
	return filepath.Join("images", "galleries",
		fmt.Sprintf("%v", collectionID))
}

func (is *imageService) mkImagePath(collectionID uint) (string, error) {
	collectionPath := is.imagePath(collectionID)
	err := os.MkdirAll(collectionPath, 0755)
	if err != nil {
		return "", err
	}
	return collectionPath, nil
}
