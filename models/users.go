package models

import (
	"regexp"
	"strings"

	"golang.org/x/crypto/bcrypt"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres" // for db interaction
	"github.com/kellydanma/memohub/hash"
	"github.com/kellydanma/memohub/rand"
)

const (
	hmacSecretKey = "totally-random-secret-hmac-key"
	userPepper    = "random-secret-text"
)

var (
	// ErrNotFound signifies that the resource does not exist.
	ErrNotFound modelError = "models: resource not found"
	// ErrIDInvalid signifies an invalid ID was provided.
	ErrIDInvalid modelError = "models: invalid ID was provided"

	// ErrRememberRequired signifies a missing user remember token hash.
	ErrRememberRequired modelError = "models: remember token is required"
	// ErrRememberTooShort signifies the remember token is not at least 32 bytes
	ErrRememberTooShort modelError = "models: remember token must be at least 32 bytes"

	// ErrPasswordInvalid signifies the wrong pw.
	ErrPasswordInvalid modelError = "models: incorrect password was provided"
	// ErrPasswordTooShort signifies a password less than 8 characters long.
	ErrPasswordTooShort modelError = "models: password must 8 characters long"
	// ErrPasswordRequired signifies a missing password.
	ErrPasswordRequired modelError = "models: password is required"

	// ErrEmailNotFound signifies a missing email address.
	ErrEmailNotFound modelError = "models: email address is required"
	// ErrEmailInvalid signifies an e-mail that doesn't satisfy the regex.
	ErrEmailInvalid modelError = "models: invalid email address"
	// ErrEmailDuplicate signifies that the address is already taken.
	ErrEmailDuplicate modelError = "models: email address is in use"
)

///////////////////////////
///                     ///
///     Interfaces      ///
///                     ///
///////////////////////////

// UserDB interacts with the users database.
type UserDB interface {
	// Methods to query single users
	ByID(id uint) (*User, error)
	ByEmail(email string) (*User, error)
	ByRemember(token string) (*User, error)

	// Methods for user CRUD
	Create(user *User) error
	Update(user *User) error
	Delete(id uint) error
}

// UserService offers methods to manipulate the
// user model.
type UserService interface {
	UserDB
	Authenticate(email, password string) (*User, error)
}

// NewUserService initiates the user service, given a db.
func NewUserService(db *gorm.DB) UserService {
	uORM := &userORM{db}
	hmac := hash.NewHMAC(hmacSecretKey)
	uv := newUserValidator(uORM, hmac)
	return &userService{
		UserDB: uv,
	}
}

///////////////////////////
///                     ///
///       Structs       ///
///                     ///
///////////////////////////

// User is a model for the "users" database.
type User struct {
	gorm.Model
	Name         string
	Email        string `gorm:"not null;unique_index"`
	Password     string `gorm:"-"`
	PasswordHash string `gorm:"not null"`
	Remember     string `gorm:"-"`
	RememberHash string `gorm:"not null;unique_index"`
}

// userService abstracts db operations and
// services as an API for querying, creating,
// and updating users.
type userService struct {
	UserDB
}

///////////////////////////
///                     ///
///       Errors        ///
///                     ///
///////////////////////////

type modelError string

func (e modelError) Error() string {
	return string(e)
}

func (e modelError) Public() string {
	msg := strings.Replace(string(e), "models: ", "", 1)
	split := strings.Split(msg, " ")
	split[0] = strings.Title(split[0])
	return strings.Join(split, " ")
}

///////////////////////////
///                     ///
///    ORM functions    ///
///                     ///
///////////////////////////

// userORM interacts with the DB and fully
// implements the UserDB interface.
type userORM struct {
	db *gorm.DB
}

// ByID looks up a user with the provided ID.
// If the user is not found, it returns ErrNotFound.
func (uo *userORM) ByID(id uint) (*User, error) {
	var user User
	db := uo.db.Where("id = ?", id)
	err := first(db, &user)
	switch err {
	case nil:
		return &user, nil
	case gorm.ErrRecordNotFound:
		return nil, ErrNotFound
	default:
		return nil, err // return all other errors
	}
}

// ByEmail looks up a user with the provided email.
// If the user is not found, it returns ErrNotFound.
func (uo *userORM) ByEmail(email string) (*User, error) {
	var user User
	db := uo.db.Where("email = ?", email)
	err := first(db, &user)
	switch err {
	case nil:
		return &user, nil
	case gorm.ErrRecordNotFound:
		return nil, ErrNotFound
	default:
		return nil, err // return all other errors
	}
}

// ByRemember looks up a user with the given remember
// token and returns that user. It expects the remember
// token to be hashed.
func (uo *userORM) ByRemember(rememberHash string) (*User, error) {
	var user User
	err := first(uo.db.Where("remember_hash = ?", rememberHash), &user)
	if err != nil {
		return nil, err
	}
	return &user, nil
}

// Create creates a validated user.
func (uo *userORM) Create(user *User) error {
	return uo.db.Create(user).Error
}

// Update updates new fields for the provided user.
func (uo *userORM) Update(user *User) error {
	return uo.db.Save(user).Error
}

// Delete deletes the user with the provided ID.
func (uo *userORM) Delete(id uint) error {
	user := User{Model: gorm.Model{ID: id}}
	return uo.db.Delete(&user).Error
}

// Authenticate authenticates a user with the
// provided email address and password.
func (us *userService) Authenticate(email, pw string) (*User, error) {
	foundUser, err := us.ByEmail(email)
	if err != nil {
		return nil, err // returns ErrNotFound
	}
	err = bcrypt.CompareHashAndPassword(
		[]byte(foundUser.PasswordHash),
		[]byte(pw+userPepper))
	switch err {
	case nil:
		return foundUser, nil
	case bcrypt.ErrMismatchedHashAndPassword:
		return nil, ErrPasswordInvalid
	default:
		return nil, err
	}
}

///////////////////////////
///                     ///
///  User validation    ///
///                     ///
///////////////////////////

// userValidator normalizes & validates data before
// passing it to the next UserDB in the interface chain.
type userValidator struct {
	UserDB
	hmac       hash.HMAC
	emailRegex *regexp.Regexp
}

// userValFn validates user data.
type userValFn func(*User) error

func newUserValidator(userDB UserDB, hmac hash.HMAC) *userValidator {
	return &userValidator{
		UserDB: userDB,
		hmac:   hmac,
		emailRegex: regexp.MustCompile( // permit subdomains in emails
			`^[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,16}$`),
	}
}

// ByRemember hashes the remember token, then calls
// ByRemember on the subsequent UserDB interface layer.
func (uv *userValidator) ByRemember(token string) (*User, error) {
	user := User{
		Remember: token,
	}
	if err := runUserValFns(&user, uv.hmacRemember); err != nil {
		return nil, err
	}
	return uv.UserDB.ByRemember(user.RememberHash)
}

// ByEmail normalizes the email before calling ByEmail
// on the subsequent UserDB interface layer.
func (uv *userValidator) ByEmail(email string) (*User, error) {
	user := User{
		Email: email,
	}
	if err := runUserValFns(&user,
		uv.normalizeEmail); err != nil {
		return nil, err
	}
	return uv.UserDB.ByEmail(user.Email)
}

// Create creates the provided user and backfills
// data: ID, CreatedAt, and UpdatedAt fields, then
// calls Create on the subsequent UserDB interface layer.
func (uv *userValidator) Create(user *User) error {
	if err := runUserValFns(user,
		uv.passwordRequired,
		uv.passwordMinLength,
		uv.bcryptPassword,
		uv.passwordHashRequired,
		uv.setNewRemember,
		uv.rememberMinBytes(32),
		uv.hmacRemember,
		uv.rememberHashRequired,
		uv.normalizeEmail,
		uv.requireEmail,
		uv.validateEmail,
		uv.availableEmail); err != nil {
		return err
	}
	return uv.UserDB.Create(user)
}

// Update hashes a remember token, if it is provided,
// then calls Update on the subsequent UserDB
// interface layer.
func (uv *userValidator) Update(user *User) error {
	if err := runUserValFns(user,
		uv.passwordMinLength,
		uv.bcryptPassword,
		uv.passwordHashRequired,
		uv.rememberMinBytes(32),
		uv.hmacRemember,
		uv.rememberHashRequired,
		uv.normalizeEmail,
		uv.requireEmail,
		uv.validateEmail,
		uv.availableEmail); err != nil {
		return err
	}
	return uv.UserDB.Update(user)
}

// Delete validates the ID to delete, then calls
// Delete on the subsequence UserDB interface layer.
func (uv *userValidator) Delete(id uint) error {
	var user User
	user.ID = id
	if err := runUserValFns(&user,
		uv.idGreaterThan(0)); err != nil {
		return err
	} // use closure to determine min ID value
	return uv.UserDB.Delete(id)
}

// bcryptPassword hashes a user's password with a
// global pepper and bcrypt (salting).
func (uv *userValidator) bcryptPassword(user *User) error {
	if user.Password == "" {
		return nil // don't run, password hasn't changed
	}
	hashedBytes, err := bcrypt.GenerateFromPassword(
		[]byte(user.Password+userPepper), bcrypt.DefaultCost)
	if err != nil {
		return err
	}
	user.PasswordHash = string(hashedBytes)
	user.Password = ""
	return nil
}

// hmacRemember verifies that the remember token was hashed.
func (uv *userValidator) hmacRemember(user *User) error {
	if user.Remember == "" {
		return nil
	}
	user.RememberHash = uv.hmac.Hash(user.RememberHash)
	return nil
}

// setNewRemember normalizes the user's Remember, setting
// it if it is empty.
func (uv *userValidator) setNewRemember(user *User) error {
	if user.Remember != "" {
		return nil
	}
	token, err := rand.RememberToken()
	if err != nil {
		return err
	}
	user.Remember = token
	return nil
}

func (uv *userValidator) rememberHashRequired(user *User) error {
	if user.RememberHash == "" {
		return ErrRememberRequired
	}
	return nil
}

func (uv *userValidator) rememberMinBytes(n uint) userValFn {
	return userValFn(func(user *User) error {
		if user.Remember == "" {
			return nil
		}
		n, err := rand.NrBytes(user.Remember)
		if err != nil {
			return err
		}
		if n < 32 {
			return ErrRememberTooShort
		}
		return nil
	})
}

// idGreaterThan is a closure that returns a validation
// function for user ID values.
//
// Eg. valFn := uv.idGreaterThan(5)
// This retrieves the minimum ID value, 5, dynamically.
func (uv *userValidator) idGreaterThan(n uint) userValFn {
	return userValFn(func(user *User) error {
		if user.ID <= n {
			return ErrIDInvalid
		}
		return nil
	})
}

func (uv *userValidator) passwordRequired(user *User) error {
	if user.Password == "" {
		return ErrPasswordRequired
	}
	return nil
}

func (uv *userValidator) passwordHashRequired(user *User) error {
	if user.PasswordHash == "" {
		return ErrPasswordRequired
	}
	return nil
}

func (uv *userValidator) passwordMinLength(user *User) error {
	if user.Password == "" {
		return nil // passwordRequired checks for empty string
	}
	if len(user.Password) < 8 {
		return ErrPasswordTooShort
	}
	return nil
}

func (uv *userValidator) validateEmail(user *User) error {
	if user.Email == "" {
		return nil // normalizeEmail handles missing emails
	}
	if !uv.emailRegex.MatchString(user.Email) {
		return ErrEmailInvalid
	}
	return nil
}

// normalEmail normalizes the user's email.
func (uv *userValidator) normalizeEmail(user *User) error {
	user.Email = strings.ToLower(user.Email)
	user.Email = strings.TrimSpace(user.Email)
	return nil
}

func (uv *userValidator) requireEmail(user *User) error {
	if user.Email == "" {
		return ErrEmailNotFound
	}
	return nil
}

func (uv *userValidator) availableEmail(user *User) error {
	existing, err := uv.ByEmail(user.Email)
	if err == ErrNotFound {
		return nil // email doesn't exist in DB yet
	}
	if err != nil {
		return err // some other error
	}
	if existing.ID != user.ID {
		return ErrEmailDuplicate // email belongs to another user
	}
	return nil
}

///////////////////////////
///                     ///
///       Helpers       ///
///                     ///
///////////////////////////

// runUserValFns iterates over any number of user
// validation functions, capturing any errors.
func runUserValFns(user *User, fns ...userValFn) error {
	for _, fn := range fns {
		if err := fn(user); err != nil {
			return err
		}
	}
	return nil
}

// first is a helper to select the first query.
func first(db *gorm.DB, dst interface{}) error {
	err := db.First(dst).Error
	if err == gorm.ErrRecordNotFound {
		return ErrNotFound
	}
	return err
}
