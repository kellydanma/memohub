package models

import "github.com/jinzhu/gorm"

// NewServices instantiates services given some db
// connection information.
func NewServices(connectInfo string) (*Services, error) {
	db, err := gorm.Open("postgres", connectInfo)
	if err != nil {
		return nil, err
	}
	db.LogMode(true)
	return &Services{
		User:       NewUserService(db),
		Collection: NewCollectionService(db),
		Image:      NewImageService(),
		db:         db,
	}, nil
}

// AutoMigrate attempts to migrate the user
// and colleciton tables.
func (s *Services) AutoMigrate() error {
	if err := s.db.AutoMigrate(&User{},
		&Collection{}).Error; err != nil {
		return err
	}
	return nil
}

// Close closes the DB connection.
func (s *Services) Close() error {
	return s.db.Close()
}

// Reset drops all tables and reconstructs them via
// automigration.
// Reset is a destructive function.
func (s *Services) Reset() error {
	if err := s.db.DropTableIfExists(&User{},
		&Collection{}).Error; err != nil {
		return err
	}
	return s.AutoMigrate()
}

// Services collects services into a single struct.
type Services struct {
	User       UserService
	Collection CollectionService
	Image      ImageService
	db         *gorm.DB
}
